package org.zalan.zalanbmi;

public enum LengthUnit {
	METER(1.0), CENTIMETER(0.01), FOOT(0.032808399), INCH(0.0254);
	
	private double factorToMeter;
	
	LengthUnit(double factorToMeter) {
		this.factorToMeter = factorToMeter;
	}
	
	public double getInMeter(double value) {
		if (value < 0) {
			throw new IllegalArgumentException("Length cannot be a negative number!");
		}
		return value * factorToMeter;
	}
}
