package org.zalan.zalanbmi;

public enum WeightUnit {
	KILOGRAM(1.0), GRAM(0.001), POUND(0.453592), OUNCE(0.0283495);
	
	private double factorToKilogram;
	WeightUnit(double factorToKilogram) {
		this.factorToKilogram = factorToKilogram;
	}
	
	public double getInKilogram(double value) {
		if (value < 0) {
			throw new IllegalArgumentException("Weight cannot be a negative number!");
		}
		return value * factorToKilogram;
	}
	
}