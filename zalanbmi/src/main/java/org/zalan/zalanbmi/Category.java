package org.zalan.zalanbmi;

public enum Category {
	SEVERE_THINNESS, MODERATE_THINNESS, MILD_THINNESS, NORMAL, OVERWEIGHT, OBESE_CLASS_1, OBESE_CLASS_2, OBESE_CLASS_3;
	
	 static Category getCategory(double bmiValue) {
		 if (bmiValue <= 0) {
			 throw new IllegalArgumentException("BMI can only be a positive number!");
		 }
		 
		if (bmiValue < 16) {
			return SEVERE_THINNESS;
		} 
		if (bmiValue < 17) {
			return Category.MODERATE_THINNESS;
		}
		if (bmiValue < 18.5) {
			return Category.MILD_THINNESS;
		}
		if (bmiValue < 25) {
			return NORMAL;
		}
		if (bmiValue < 30) {
			return OVERWEIGHT;
		}
 		if (bmiValue < 35) {
 			return OBESE_CLASS_1;
 		}
 		if (bmiValue < 40) {
 			return OBESE_CLASS_2;
 		}
 		return Category.OBESE_CLASS_3;
	}
	
}

