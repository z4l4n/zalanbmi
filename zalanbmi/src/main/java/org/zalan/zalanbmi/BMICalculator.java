package org.zalan.zalanbmi;



public class BMICalculator {

	public static  BMIIndex calculate(LengthUnit lUnit, double lValue, WeightUnit wUnit, double wValue) {
		if (lValue <= 0) {
			throw new IllegalArgumentException("Length must be a positive number");
		}
		if (wValue <= 0) {
			throw new IllegalArgumentException("Weight must be a positive number");
		}
		double bmiValue = 0.0;
		
		//converting to meters and kilograms
		lValue = lUnit.getInMeter(lValue);
		wValue = wUnit.getInKilogram(wValue);
		
		
		bmiValue = wValue / (lValue * lValue);
		
		return new BMIIndex(bmiValue, Category.getCategory(bmiValue));
	}
	
}
