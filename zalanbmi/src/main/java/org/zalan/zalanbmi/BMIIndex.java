package org.zalan.zalanbmi;

/**
* Simple container class
*/
public class BMIIndex {

	private double bmiValue;
	private Category categoryValue;
	
	
	
	public BMIIndex(double bmiValue, Category categoryValue) {
		super();
		this.bmiValue = bmiValue;
		this.categoryValue = categoryValue;
	}

	public double getBmiValue() {
		return bmiValue;
	}
	
	public Category getCategoryValue() {
		return categoryValue;
	}
	
	
	
}
