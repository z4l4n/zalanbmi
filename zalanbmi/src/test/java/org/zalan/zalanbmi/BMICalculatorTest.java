package org.zalan.zalanbmi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BMICalculatorTest {

	private BMIIndex index;
	
	
	//positive test cases
	@Test
	public void testCalculateWithNormalKiloAndMeterValues() {
		index = BMICalculator.calculate(LengthUnit.METER, 1.7, WeightUnit.KILOGRAM, 80.0);
		assertEquals(index.getBmiValue(), 27.68, 0.01);
	}
	
	@Test
	public void testCalculateWithNormalPoundAndInchValues() {
		index = BMICalculator.calculate(LengthUnit.INCH, 70, WeightUnit.POUND, 200);
		assertEquals(index.getBmiValue(), 28.7, 0.01);
	}
	
	@Test
	public void testCalculateWithNormalMixedUnits() {
		index = BMICalculator.calculate(LengthUnit.INCH, 78.74015748, WeightUnit.KILOGRAM, 80);
		assertEquals(index.getBmiValue(), 20.0, 0.01);
	}
	
	
	
	//negative test cases
	@Test(expected = IllegalArgumentException.class)
	public void testCalculateWithZeroLength() {
		index = BMICalculator.calculate(LengthUnit.INCH, 0, WeightUnit.KILOGRAM, 80);
	}

	
	@Test(expected = IllegalArgumentException.class)
	public void testCalculateWithZeroWeight() {
		index = BMICalculator.calculate(LengthUnit.INCH, 80, WeightUnit.KILOGRAM, 0);
	}
	
	//Lack of time.. :(

}
